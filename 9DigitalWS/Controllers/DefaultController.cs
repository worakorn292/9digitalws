﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;
using _9DigitalWS.Models;
using System.Web.Http.Controllers;
using System.Threading.Tasks;
using System.Web.Http.Cors;

namespace _9DigitalWS.Controllers
{

    public class DefaultController : ApiController
    {
        public class ValidateModelStateAttribute : ActionFilterAttribute
        {
            public override void OnActionExecuting(HttpActionContext actionContext)
            {
                HttpError myCustomError = new HttpError() { { "error", "Could not decode request: JSON parsing failed" } };
                Exception ex = new Exception();
                if (!actionContext.ModelState.IsValid)
                {
                    actionContext.Response = actionContext.Request.CreateErrorResponse(
                        HttpStatusCode.BadRequest, myCustomError);
                }
            }
        }

        [ValidateModelState]
        [HttpPost]
        public ResponseModel GetSlug([FromBody] RequestModel req)
        {

                ResponseModel res = new ResponseModel();
                res.response = new List<ResponseDetail>();
                foreach (PayloadDetails pld in req.payload)
                {
                    if (pld.drm == true && pld.episodeCount > 0)
                    {

                        ResponseDetail rd = new ResponseDetail();
                        rd.image = pld.image.showImage;
                        rd.slug = pld.slug;
                        rd.title = pld.title;
                        res.response.Add(rd);
                    }

                }

                return res;
        }
    }
}
