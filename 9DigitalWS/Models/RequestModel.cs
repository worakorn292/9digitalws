﻿using System.Collections.Generic;


namespace _9DigitalWS.Models
{
    public class RequestModel
    {

        public List<PayloadDetails> payload { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
        public int totalRecords { get; set; }
    }

    public class PayloadDetails
    {

        public string country { get; set; }
        public string description { get; set; }
        public bool drm { get; set; }
        public int episodeCount { get; set; }
        public string genre { get; set; }
        public Images image {get; set;}
        public string language { get; set; }
        public Episode nextEpisode { get; set; }
        public string primaryColour { get; set; }
        public List<Season> seasons { get; set; }
        public string slug { get; set; }
        public string title { get; set; }
        public string tvChannel { get; set; }

    }

    public class Images
    {
        public string showImage {get; set;}
    }

    public class Episode
    {
        public string channel { get; set; }
        public string channelLogo { get; set; }
        public string date { get; set; }
        public string html { get; set; }
        public string url { get; set; }
    }

    public class Season
    {
        public string slug { get; set; }
    }}