﻿using System.Collections.Generic;

namespace _9DigitalWS.Models
{
    public class ResponseModel
    {
        public List<ResponseDetail> response { get; set; }
    }

    public class ResponseDetail
    {
        public string image { get; set; }
        public string slug { get; set; }
        public string title { get; set; }
    }
}